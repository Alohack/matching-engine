// This file implemets MatchEngine functionality.
#include "MatchEngine.h"
#include <regex>
#include <stdexcept>
#include <sstream>
#include <ranges>

namespace
{
	/*
	* returns a '+' for a buying order and '-' for selling.
	*/
	char sign(const Order& order)
	{
		return order.is_buying ? '+' : '-';
	}

	/*
	* This comparator compares 2 trades by trader, sign and price (in mentioned order).
	* It is used to traverse the trades in the order mentioned in the last line
	* of the problem statement for printing the trades.
	*/
	struct TradesComparator
	{
		bool operator()(const Order& lhs, const Order& rhs) const
		{
			if (lhs.trader_id != rhs.trader_id)
				return lhs.trader_id < rhs.trader_id;
			if (sign(lhs) != sign(rhs))
				return sign(lhs) < sign(rhs);
			return lhs.price < rhs.price;
		}
	};

	/*
	* This function takes output stream as an argument and a vector of orders
	*
	* printTrades outputs the trades in the format mentioned in the problem statement.
	* Several trades of one trader with the same side and price, created on one aggressor
	* execution, are reported as one trade with cumulative quantity.
	*/
	void printTrades(std::ostream& out, const std::vector<Order>& trades)
	{
		if (trades.empty()) 
			return;

		std::map<Order, std::size_t, TradesComparator> sorted_trades;
		for (const auto& order : trades)
			sorted_trades[order] += order.qty;

		const auto printTrade = [](std::ostream& out, 
			const Order& trade, std::size_t qty)
		{
			out << trade.trader_id << sign(trade) << qty << "@" << trade.price;
		};

		printTrade(out, sorted_trades.begin()->first, sorted_trades.begin()->second);
		for (const auto& [trade, qty] : sorted_trades | std::views::drop(1))
			printTrade(out << " ", trade, qty);
		out << "\n";
		return;
	}
}

/*
* The run function inputs aggressor as long as there are still
* requests left in the input stream. It then tries to match it against resting orders
* of the opposing side and outputs the result with printTrades function.
*
* Aggressors must be inputed by the format given in the problem statement, 
* otherwise the program will throw a runtime error.
*/
void MatchEngine::run(std::istream& in, std::ostream& out)
{
	static const std::regex request_regex("[a-zA-Z0-9]+ (B|S) \\d+ \\d+");

	for (std::string line; std::getline(in, line); line = {})
	{
		if (line.empty())
			continue;

		if (!std::regex_match(line, request_regex))
			throw std::runtime_error("A line didn't match the format of a request.");

		Order aggressor;
		std::istringstream iss{std::move(line)};
		iss >> aggressor;

		const std::vector<Order> trades = request_matcher.match(std::move(aggressor));
		if (trades.empty())
			continue;
		printTrades(out, trades);
	}
}
