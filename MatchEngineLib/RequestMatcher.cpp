// This file implements RequestMatcher functionality.
#include "RequestMatcher.h"
#include <vector>

namespace
{
	/*
	* This function takes a request as an argument, its opposing side request matcher 
	* and a request matcher of the same side.
	*
	* Matches the given request to orders of the given opposing_requst_matcher, if the 
	* request isn't entirely matched then the request is added to the orders of request_matcher.
	*/
	template <typename OpposingRequestMatcher, typename RequestMatcher>
	std::vector<Order> match_side_request(OpposingRequestMatcher& opposing_requst_matcher,
		RequestMatcher& request_matcher, Order& request)
	{
		std::vector<Order> trades = opposing_requst_matcher.match(request);
		if (request.qty > 0)
			request_matcher.add(request);
		return trades;
	}
}

/*
* Matches the given request to its opposing side (If the request isn't exhausted it is
* added to the resting orders of the same side).
*/
std::vector<Order> RequestMatcher::match(Order request)
{
	if (request.is_buying)
		return match_side_request(selling_request_matcher, buying_request_matcher, request);
	return match_side_request(buying_request_matcher, selling_request_matcher, request);
}
