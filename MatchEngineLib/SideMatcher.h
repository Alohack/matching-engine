#pragma once
#include "Order.h"
#include <map>
#include <vector>

/*
* SideMatcher is used to match only requests with a single side,
* e.g. to match a given buying request only to the resting selling orders.
*
* It contains the resting orders only of a single side.
*
* If the Comparator is std::greater<> then the SideMatcher
* will contain buying resting orders and will match selling requests.
* If the Comparator is std::less<> then the SideMatcher
* will contain selling resting orders and will match buying requests.
*/

template <typename Comparator>
class SideMatcher
{
private:
	/*
	* Resting orders of one side are stored in a multimap. The keys are
	* the prices of the requests.
	*/
	std::multimap<std::size_t, Order, Comparator> side_orders;

public:
	/*
	* The match function matches the given request with the
	* resting orders of the opposite side stored in side_orders.
	*
	* The function subtracts the total quantity of matched orders
	* from the given request and removes all resting orders that found a match.
	* The resulting trades are returned as a vector of orders.
	*/
	std::vector<Order> match(Order& request)
	{
		if (0 == request.qty)
			return {};

		const auto finish = side_orders.upper_bound(request.price);
		if (side_orders.begin() == finish)
			return {};

		std::vector<Order> matched_orders;
		auto it = side_orders.begin();
		for (; it != finish && request.qty > 0; ++it)
		{
			const std::size_t min_qty = std::min(it->second.qty, request.qty);

			matched_orders.push_back(it->second);
			matched_orders.back().qty = min_qty;

			matched_orders.push_back(request);
			matched_orders.back().qty = min_qty;
			matched_orders.back().price = it->second.price;

			request.qty -= min_qty;
			it->second.qty -= min_qty;
		}

		const auto prev = std::prev(it);
		side_orders.erase(side_orders.begin(), 0 == prev->second.qty ? it : prev);

		return matched_orders;
	}

	/*
	* adds the given request to the resting orders of the same side.
	*/
	void add(Order request)
	{
		side_orders.emplace(request.price, std::move(request));
	}
};
