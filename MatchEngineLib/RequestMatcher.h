#pragma once
#include "SideMatcher.h"

/*
* RequestMatcher is used to match a given request to the resting orders
* of its opposing side, i.e. it can process both scenarios:
* - when the request is a buying request it is matched against selling resting orders,
* - when the request is a selling request it is matched against buying resting orders.
*
* After that, if the requests quantity isn't exhausted, the remaining request
* is added to the resting orders of the same side.
*/

class RequestMatcher
{
private:
	using BuyingRequestMatcher = SideMatcher<std::greater<>>;
	using SellingRequestMatcher = SideMatcher<std::less<>>;

	BuyingRequestMatcher buying_request_matcher;
	SellingRequestMatcher selling_request_matcher;

public:
	std::vector<Order> match(Order request);
};
