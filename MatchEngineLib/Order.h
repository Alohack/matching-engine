#pragma once
#include <string>

/*
* A single order is either a request or a trade.
* They are represented with this same class since they have the same signature.
*
* Each order contains the following information:
* 1. trader identifier,
* 2. is it a buying or a selling order,
* 3. quantity of the units in the order,
* 4. price of a single unit suggested in the order.
*/

struct Order
{
	std::string trader_id{};
	bool is_buying{};
	std::size_t qty{};
	std::size_t price{};

	friend std::istream& operator>>(std::istream& in, Order& request);
};
