#pragma once
#include <iostream>
#include <vector>
#include "RequestMatcher.h"

/*
* Match engine contains only a single object - request matcher,
* and has only one member function - run.
*
* It is used to read orders from the input stream, match the input request
* to the resting orders of request_matcher and write the resulting trades
* to the output stream.
*/

class MatchEngine
{
private:
	RequestMatcher request_matcher;
public:
	void run(std::istream& in, std::ostream& out);
};
