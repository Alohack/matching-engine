// This file implements the functionality for the Order structure.
#include "Order.h"
#include <iostream>

/*
* This function takes an input stream and a request as arguments.
*
* The input must represent
*
* <Trader Identifier> <Side> <Quantity> <Price>,
*
* where
* <Trader Identifier> is an alpha-numeric string,
* <Side> is a single char: 'B' if this is a buy request, and 'S' if this is a sell request,
* <Quantity> is an integer size of a request,
* <Price> is an integer price of request.
*
*/

std::istream& operator>>(std::istream& in, Order& request)
{
	char side{};
	in >> request.trader_id >> side >> request.qty >> request.price;
	request.is_buying = (side == 'B');
	return in;
}
