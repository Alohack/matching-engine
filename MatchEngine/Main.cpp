// Main file.
#include <iostream>
#include <exception>
#include <MatchEngine.h>

/*
* Our main function simply creates an instance of a MatchEngine.
* Since The task was to read from stdin and write to stdout,
* we pass std::cin and std::cout to the member function - run.
*/

int main()
try
{
	MatchEngine match_engine;
	match_engine.run(std::cin, std::cout);
	return 0;
}
catch (const std::exception& error)
{
	std::cerr << "ERROR: " << error.what() << "\n";
	return -1;
}
catch (...)
{
	std::cerr << "UNKNOWN ERROR\n";
	return -1;
}