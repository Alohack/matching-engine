#include "pch.h"
#include <MatchEngine.h>

namespace
{
	/*
	*  input contains all the requests that we want to test, 
	*  expected_output is the output that must be produced by the input.
	* 
	*  This function runs the matching engine for the given input and
	*  compairs its output with the expected output.
	*/
	void testMatchEngine(std::string input, std::string_view expected_output)
	{
		std::istringstream input_stream{std::move(input)};
		std::ostringstream output_stream;

		MatchEngine match_engine;
		match_engine.run(input_stream, output_stream);

		const std::string output = output_stream.str();
		ASSERT_EQ(expected_output, output);
	}
}

// Tests the case of empty input.
TEST(MatchEngineTest, Empty)
{
	testMatchEngine("", "");
}

// Tests the case given in the problem statement.
TEST(MatchEngineTest, General)
{
	testMatchEngine(
R"(T1 B 5 30
T2 S 5 70
T3 B 1 40
T4 S 2 60
T5 S 3 70
T6 S 20 80
T7 S 1 50
T2 S 5 70
T1 B 1 50
T1 B 3 60
T7 S 2 50
T8 B 10 90
)",
R"(T1+1@50 T7-1@50
T1+2@60 T4-2@60
T1+1@60 T7-1@60
T2-6@70 T5-3@70 T7-1@50 T8+1@50 T8+9@70
)");
}

// Tests the case when a wrong input is given.
TEST(MatchEngineTest, WrongInput)
{
	ASSERT_THROW(testMatchEngine("T1 Buying 3 40", ""), std::runtime_error);
}